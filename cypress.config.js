const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    specPattern: 'cypress/e2e/**/*.cy.{js,jsx,ts,tsx}', // Adjust this pattern if needed
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
  chromeWebSecurity: false,
  firefoxOptions: {
    prefs: {
      'profile': 'cypress-profile',
    },
  },
  e2e: {
    baseUrlEpam: 'https://www.epam.com',
    baseUrlDWS: 'https://demowebshop.tricentis.com/',
    baseUrlPetSore: 'https://petstore.swagger.io/v2',
    baseUrlUAorigin: 'https://careers.epam.ua/',
    existDownloadedFile: './cypress/downloads/EPAM_Corporate_Overview_Q3_october.pdf',
  },
});
