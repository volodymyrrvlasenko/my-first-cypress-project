// cypress/integration/epam.spec.js

describe('EPAM Test', () => {
    it('should have the correct title', () => {
      // Open EPAM.com
      cy.visit('https://www.epam.com');
  
      // Compare the title
      cy.title().should('eq', 'EPAM | Software Engineering & Product Development Services');
    });
  });
  