import LoginForm from './components/LoginForm.js';
class LoginPageDWS {
    loginForm = new LoginForm;
    get headerLinks() { 
        return cy.get('.header-links') 
    };
    get submitLoginButton() { 
        return cy.get('.login-button') 
    }; 
}

export default LoginPageDWS;