class ShoppingCart {
    get CheckOutButton() { 
        return cy.get('#checkout') 
    };
    get agrimentCheckBox() { 
        return cy.get('#termsofservice') 
    };
    get updateCartButton() { 
        return cy.get('.update-cart-button') 
    };
    get ItemCheckBox() { 
        return cy.get('.remove-from-cart > input') 
    };
    get shoppingItemsQty() { 
        return cy.get('.cart-qty') 
    };
}

export default ShoppingCart;