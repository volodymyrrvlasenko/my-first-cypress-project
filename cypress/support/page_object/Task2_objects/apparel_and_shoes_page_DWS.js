import 'cypress-xpath';
class ApparelAndShoesPageDWS {
    get blueAndGreenSneaker() { 
        return cy.xpath("//a[normalize-space()='Blue and green Sneaker']") 
    };
}

export default ApparelAndShoesPageDWS;