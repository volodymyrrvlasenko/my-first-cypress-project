class BlueSneaker {
    get addToWishListButton() { 
        return cy.get('#add-to-wishlist-button-28') 
    };
    get addToCartButton() { 
        return cy.get('#add-to-cart-button-28') 
    };
}

export default BlueSneaker;