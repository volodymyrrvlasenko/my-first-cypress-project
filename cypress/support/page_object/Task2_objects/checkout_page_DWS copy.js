class Checkout {
    get resultText() { 
        return cy.get('strong') 
    };
    get confirmButton() { 
        return cy.get('#confirm-order-buttons-container > input') 
    };
    get PayInfoCointinueButton() { 
        return cy.get('#payment-info-buttons-container > input') };
    get PayMethodCointinueButton() { return cy.get('#payment-method-buttons-container > input') 
};
    get ShipMethodCointinueButton() { 
        return cy.get('#shipping-method-buttons-container > input') 
    };
    get ShipAddressCointinueButton() { 
        return cy.get('#shipping-buttons-container > input') 
    };
    get BillAddressCointinueButton() { 
        return cy.get('#billing-buttons-container > input') 
    };

}

export default Checkout;