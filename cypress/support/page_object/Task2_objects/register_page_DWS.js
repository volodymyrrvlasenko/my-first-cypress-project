import RegisterForm from './components/RegisterForm.js';
class RegisterPageDWS {
    registerForm = new RegisterForm;
    get registerResult() { 
        return cy.get('.result') 
    };
    get submitRegisteButton() { 
        return cy.get('#register-button') 
    };
}

export default RegisterPageDWS;