import 'cypress-xpath';
class HomePageDWS {
    get shopCartLink() { 
        return cy.xpath("//li[@id='topcartlink']//a[@class='ico-cart']") 
    };
    get wishlistLink() { 
        return cy.xpath("//div[@class='header-links']//a[@class='ico-wishlist']") 
    };
    get wishlistLinkQuantity() { 
        return cy.get('.wishlist-qty') 
    };
    get ShopCartQuantity() { 
        return cy.get('.cart-qty') 
    };
    get selectProductPageSize() { 
        return cy.get('#products-pagesize') 
    };
    get numberOfItems() { 
        return cy.get('.product-item') 
    };
    get apparelAndShoes() { 
        return cy.xpath("//ul[@class='top-menu']//a[normalize-space()='Apparel & Shoes']") 
    };
    get booksProductItems() { 
        return cy.get('.product-item') 
    };
    get sortningDropdown() { 
        return cy.get('#products-orderby') 
    };
    get booksTab() { 
        return cy.xpath("//ul[@class='top-menu']//a[normalize-space()='Books']") 
    };
    get subGroups() { 
        return cy.get('.sublist.firstLevel.active li a') 
    };
    get topMenu() { 
        return cy.get('.top-menu') 
    };
    get loginLink() { 
        return cy.get('.ico-login') 
    };
    get registerLink() { 
        return cy.get('.ico-register') 
    };
    open() {
        cy.visit('https://demowebshop.tricentis.com/');
    };

}

export default HomePageDWS;