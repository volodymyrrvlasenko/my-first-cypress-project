class LoginForm {
    get loginPassword() { 
      return cy.get('#Password') 
    };
    get loginEmail() { 
      return cy.get('#Email') 
    };
    
    fill(options) {
      if (options.loginEmail) {
        this.loginEmail.type(options.loginEmail);
      }
      if (options.loginPassword) {
        this.loginPassword.type(options.loginPassword);
      }
    }
}

export default LoginForm;