const faker = require('faker');
class RegisterForm {
    
  get getConfirmPassword() { 
    return cy.get('#ConfirmPassword') 
  };
  get getPassword() { 
    return cy.get('#Password') };
  get getEmail() { return cy.get('#Email') 
};
  get getLastName() { 
    return cy.get('#LastName') 
  };
  get getFirstName() { 
    return cy.get('#FirstName') 
  };
  get getGenderMale() { 
    return cy.get('#gender-male') 
  };
  get getGenderFemale() { 
    return cy.get('#gender-female') 
  };
    
    fill(options) {
      if (options.getGenderMale) {
        this.getGenderMale.type(options.getGenderMale).click();
      }
      if (options.getGenderFemale) {
        this.getGenderFemale.type(options.getGenderFemale).click();
      }
      if (options.getFirstName) {
        this.getFirstName.type(options.getFirstName);
      }
      if (options.getLastName) {
        this.getLastName.type(options.getLastName);
      }
      if (options.getEmail) {
        this.getEmail.type(faker.internet.email());
      }
      if (options.getPassword) {
        this.getPassword.type(options.getPassword);
      }
      if (options.getConfirmPassword) {
        this.getConfirmPassword.type(options.getConfirmPassword);
      }
    }
}

export default RegisterForm;