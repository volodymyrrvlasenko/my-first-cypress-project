class WishListPageDWS {
    get wishListQuantity() { 
        return cy.get('.wishlist-qty') 
    };
}

export default WishListPageDWS;