const updatedPet = {
    id: 100,
    category: {
      id: 0,
      name: "Perec"
    },
    name: "doggie",
    photoUrls: [
      "string"
    ],
    tags: [
      {
        id: 0,
        name: "string"
      }
    ],
    status: "updated"
  };

export default updatedPet