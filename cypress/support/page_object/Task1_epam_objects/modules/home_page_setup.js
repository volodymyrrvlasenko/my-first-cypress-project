import HomePage from "../home_page.js";

export const createHomePageSetup = () => {
        const homePage = new HomePage();
        homePage.open();
        cy.viewport(1280, 720);
        homePage.acceptCookiesButton.click();
};
