// // Check Contacts objects
// export const acceptAll = '#onetrust-accept-btn-handler';
// export const contactUsButton = '#wrapper > div.header-container.iparsys.parsys > div.header.section > header > div > div > a.cta-button-ui.cta-button-ui-23.header__control';
// export const addressDetails = '.column-control .colctrl-ui-23 .text-ui-23 p.scaling-of-text-wrapper';
// export const phoneNumbers = '.column-control .colctrl-ui-23 .text-ui-23 p.scaling-of-text-wrapper';

// Check localization
// export const languagePanel = '#wrapper > div.header-container.iparsys.parsys > div.header.section > header > div > div > ul > li:nth-child(2) > div > div > button > span';
// export const UAoption = '#wrapper > div.header-container.iparsys.parsys > div.header.section > header > div > div > ul > li:nth-child(2) > div > nav > ul > li:nth-child(6) > a';

// Check logo
// export const getLogo = '#wrapper > div.header-container.iparsys.parsys > div.header.section > header > div > div > a.header__logo-container.desktop-logo';

// Check Our Locations
// export const getAmericas = '.tabs-23__link[data-item="0"]';
// export const getEmea = '.tabs-23__link[data-item="1"]';
// export const getApac = '.tabs-23__link[data-item="2"]';
// export const selectAmericas = '#id-c1e3377f-1d1e-3625-baf5-08d66e30c4aa > div.js-tabs-controls > div > div > div.tabs-23__title.js-tabs-title.active > a';
// export const selectEmea = '#id-c1e3377f-1d1e-3625-baf5-08d66e30c4aa > div.js-tabs-controls > div > div > div:nth-child(2) > a';
// export const selectApac = '#id-c1e3377f-1d1e-3625-baf5-08d66e30c4aa > div.js-tabs-controls > div > div > div:nth-child(3) > a';

// Check EPAM Polocies
// export const footerEpam = 'footer.footer-ui-23';
// export const policiesEpam = '.policies';

// Check report download
// export const getAboutTab = 'a.top-navigation__item-link[href="/about"]';
// export const getReportSection = '.scroll-infographic-ui-23.vertical-scroll';
// export const fileForDownload = 'EPAM_Corporate_Overview_Q3_october.pdf';
// const interceptorAddress = 'https://www.epam.com/content/dam/epam/free_library';
// export const downloadsFolder = 'C:/Users/Volodymyr_Vlasenko/Documents/Grow/JS_for_testers/Cypress_project/cypress/downloads';
// export const getDownloadButton = '#main > div.content-container.parsys > div:nth-child(5) > section > div.section__wrapper.section--padding-no > div > div > div:nth-child(1) > div > div.button > div > a > span';
// export const existDownloadedFile = 'C:/Users/Volodymyr_Vlasenko/Documents/Grow/JS_for_testers/Cypress_project/cypress/downloads/EPAM_Corporate_Overview_Q3_october.pdf';
// export const reportName = 'EPAM_Corporate_Overview_Q3_october.pdf'

// Search check
// export const getSearchButton = '.header-search__button';
// export const getSearchInput = '.header-search__input';
// export const getSubmitButton = '.custom-search-button';
// export const getSearchResult = '.search-results__counter';

// Theme check
// export const getThemeState = '.theme-switcher-label.body-text-small';
// export const getThemeToggle = 'a.desktop-logo + section.theme-switcher-ui .switch';

