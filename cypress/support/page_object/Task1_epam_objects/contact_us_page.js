import 'cypress-xpath';
class ContactUsPage {
    get addressDetails() { 
        return cy.get(':nth-child(2) > .text-ui-23') 
    };
    get selectApac() { 
        return cy.xpath("//a[normalize-space()='APAC']") 
    };
    get selectEmea() { 
        return cy.xpath("//a[normalize-space()='EMEA']") 
    };
    get selectAmericas() { 
        return cy.xpath("//a[normalize-space()='AMERICAS']")
    };
}

export default ContactUsPage;