import 'cypress-xpath';
class AboutPage {
    get DownloadButton() { 
        return cy.xpath("//div[@class='colctrl__holder']//span[@class='button__inner']") 
    };
    get ReportSection() { 
        return cy.get('.scroll-infographic-ui-23.vertical-scroll') 
    };
}

export default AboutPage;