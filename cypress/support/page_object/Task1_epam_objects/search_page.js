class SearchPage {
    get searchResult() { 
        return cy.get('.search-results__counter') 
    };
    get submitButton() { 
        return cy.get('.custom-search-button') 
    };
    get searchInput() { 
        return cy.get('.header-search__input') 
    };
}

export default SearchPage;