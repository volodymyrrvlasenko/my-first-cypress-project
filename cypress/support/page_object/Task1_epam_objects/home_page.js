import 'cypress-xpath';
class HomePage {
    get themeToggle() { 
        return cy.get('a.desktop-logo + section.theme-switcher-ui .switch') 
    };
    get themeState() { 
        return cy.get('.theme-switcher-label.body-text-small') 
    };
    get uaOption() { 
        return cy.xpath("//a[@class='location-selector__link'][contains(text(),'Україна')]") 
    };
    get languagePanel() { 
        return cy.get('.location-selector__button') 
    };
    get aboutTab() { 
        return cy.xpath("//a[@class='top-navigation__item-link js-op'][normalize-space()='About']") 
    };
    get contactUsButton() { 
        return cy.get('.header__content > :nth-child(5)') 
    };
    get logoEpam() { 
        return cy.get('.desktop-logo > .header__logo-light') 
    };
    get searchIcon() { 
        return cy.get('.header-search__button') 
    };
    get policesList() { 
        return cy.get('.policies') 
    };
    get footer() { 
        return cy.get('footer.footer-ui-23') 
    };
    get acceptCookiesButton() { 
        return cy.get('#onetrust-accept-btn-handler') 
    };
    open() {
        cy.visit('https://www.epam.com');
    };
};

export default HomePage;