import HomePage from '../../support/page_object/Task1_epam_objects/home_page.js';
import { createHomePageSetup } from '../../support/page_object/Task1_epam_objects/modules/home_page_setup.js';

describe('EPAM.com - Check contact us details', () => {
  let homePage;
  beforeEach(() => {
    homePage = new HomePage();
    createHomePageSetup();
  });

  it('should lead to the main page when clicking on the Company Logo', () => {
    // Click on the company logo
    homePage.logoEpam.click();

    // Verify the URL is the main page
    cy.url().should('eq', 'https://www.epam.com/');
  });
});
