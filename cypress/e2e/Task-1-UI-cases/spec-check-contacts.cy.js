import HomePage from '../../support/page_object/Task1_epam_objects/home_page.js';
import ContactUsPage from '../../support/page_object/Task1_epam_objects/contact_us_page.js';
import { createHomePageSetup } from '../../support/page_object/Task1_epam_objects/modules/home_page_setup.js';

describe('EPAM.com - Check contact us details', () => {
  let homePage;
  let contactUsPage;

  beforeEach(() => {
    homePage = new HomePage();
    contactUsPage = new ContactUsPage();
    createHomePageSetup();
  });

  it('should have the correct GLOBAL HEADQUARTERS details', () => {
    // Click contact us button
    homePage.contactUsButton.click();

    // Check the address
    contactUsPage.addressDetails
      .should('contain.text', '41 University Drive • Suite 202,')
      .and('contain.text', 'Newtown, PA 18940 • USA')
      // And phone numbers
      .and('contain.text', 'P +1-267-759-9000')
      .and('contain.text', 'F +1-267-759-8989');
  });
});
