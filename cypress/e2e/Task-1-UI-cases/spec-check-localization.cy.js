import HomePage from '../../support/page_object/Task1_epam_objects/home_page.js';
const { baseUrlUAorigin } = Cypress.config();
import { createHomePageSetup } from '../../support/page_object/Task1_epam_objects/modules/home_page_setup.js';

describe('EPAM Test', () => {
  let homePage;
  beforeEach(() => {
    homePage = new HomePage();
    createHomePageSetup();
  });
  it('should switch site language to Ukraine (UA)', () => {
    // Open Select a language panel
    homePage.languagePanel.click();
    
    // Click the UA option
    homePage.uaOption.should('be.visible').click();

    cy.origin(baseUrlUAorigin, () => {
      cy.url().should('contain', 'careers.epam.ua')
    });    
  });
});
