import { createHomePageSetup } from '../../support/page_object/Task1_epam_objects/modules/home_page_setup.js';

describe('EPAM Test', () => {
  beforeEach(() => {
    createHomePageSetup();
  });

  it('should have the correct title', () => {
    // Compare the title
    cy.title().should('eq', 'EPAM | Software Engineering & Product Development Services');
  });
});
