import HomePage from '../../support/page_object/Task1_epam_objects/home_page.js';
import ContactUsPage from '../../support/page_object/Task1_epam_objects/contact_us_page.js';
import { createHomePageSetup } from '../../support/page_object/Task1_epam_objects/modules/home_page_setup.js';

describe('EPAM.com - Check Our Locations', () => {
  let homePage;
  let contactUsPage;
  beforeEach(() => {
    homePage = new HomePage();
    contactUsPage = new ContactUsPage();
    createHomePageSetup();
  }); 

  it('should have the correct GLOBAL HEADQUARTERS details', () => {
    // Click contact us button
    homePage.contactUsButton.click();

    //Assert AMERICAS region
    contactUsPage.selectAmericas.click();
    contactUsPage.selectAmericas.should('have.attr', 'data-item', '0').should('have.attr', 'aria-selected', 'true');
  });
});
