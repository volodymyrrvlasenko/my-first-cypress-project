import HomePage from '../../support/page_object/Task1_epam_objects/home_page.js';
import AboutPage from '../../support/page_object/Task1_epam_objects/about_page.js';
const { existDownloadedFile } = Cypress.config();
import { createHomePageSetup } from '../../support/page_object/Task1_epam_objects/modules/home_page_setup.js';

describe('EPAM.com - Check EPAM Corporate Overview download', () => {
  let homePage;
  let aboutPage;
  beforeEach(() => {
    homePage = new HomePage();
    aboutPage = new AboutPage();
    createHomePageSetup();
  }); 

  it('should download the EPAM Corporate Overview 2023 report from EPAM at a Glance', () => {
   // Navigate to the About tab
    homePage.aboutTab.click();

    // Click the download button
    aboutPage.DownloadButton.should('be.visible').click();

    // Check that the file exists in the downloads folder
    cy.readFile(existDownloadedFile).should('exist');
  });
});
