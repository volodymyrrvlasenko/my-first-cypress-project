import HomePage from '../../support/page_object/Task1_epam_objects/home_page.js';
import { createHomePageSetup } from '../../support/page_object/Task1_epam_objects/modules/home_page_setup.js';

describe('EPAM Website Theme Switching', () => {
  let homePage;
  beforeEach(() => {
    homePage = new HomePage();
    createHomePageSetup();
  });

  it('should change theme to opposite state', () => {

    // Get the initial theme state
    homePage.themeState.should('contain.text', 'Dark Mode');

    // Click the theme toggle
    homePage.themeToggle.trigger('click');

    // Verify the changed theme state
    homePage.themeState.should('contain.text', 'Light Mode'); // Verify presence of dark theme class
  });
});
