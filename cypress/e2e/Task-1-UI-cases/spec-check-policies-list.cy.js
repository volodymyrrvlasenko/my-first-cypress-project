import HomePage from '../../support/page_object/Task1_epam_objects/home_page.js';
import expectedPolicies from '../../support/page_object/Task1_epam_objects/expected_policies_items.js';
import { createHomePageSetup } from '../../support/page_object/Task1_epam_objects/modules/home_page_setup.js';

describe('EPAM.com - Check Policies List', () => {
  let homePage;
  beforeEach(() => {
    homePage = new HomePage();
    createHomePageSetup();
  }); 

  it('should include the expected items in the policies list', () => {
    // Scroll to the bottom of the page
    homePage.footer.scrollIntoView({ duration: 2000 });

    // Get the policies list items
    homePage.policesList;

    // Check if each expected policy is present in the list
    expectedPolicies.forEach((policy) => {
      homePage.policesList.should('contain.text', policy);
    });
  });
});
