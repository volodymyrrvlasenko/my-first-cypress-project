import HomePage from '../../support/page_object/Task1_epam_objects/home_page.js';
import SearchPage from '../../support/page_object/Task1_epam_objects/search_page.js';
import { createHomePageSetup } from '../../support/page_object/Task1_epam_objects/modules/home_page_setup.js';

describe('EPAM.com - Check Search Function', () => {
  let homePage;
  let searchPage;
  beforeEach(() => {
    homePage = new HomePage();
    searchPage = new SearchPage();
    createHomePageSetup();
  });

  it('should show search results for "AI"', () => {

    // Open the search field
    cy.get('.header-search__button').click();

    // Type "AI" into the search input
    searchPage.searchInput.type('AI');

    // Submit the search form
    searchPage.submitButton.click();

    // Check if the search results are displayed
    searchPage.searchResult.should('contain.text', 'results for "AI"');
  });
});
