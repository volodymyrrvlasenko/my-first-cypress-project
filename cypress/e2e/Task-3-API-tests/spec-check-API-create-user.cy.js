const { baseUrlPetSore } = Cypress.config();
import user from "../../support/page_object/Task3_API_objects/user_object";
describe('Pet Store API Tests', () => {

  it('allows creating a User', () => {

    cy.request('POST', `${baseUrlPetSore}/user`, user).then((response) => {
      expect(response.status).to.eq(200);
    });
  });
});