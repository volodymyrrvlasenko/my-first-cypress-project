const { baseUrlPetSore } = Cypress.config();

describe('Pet Store API Tests', () => {

  it('allows Log out User', () => {
    cy.request('GET', `${baseUrlPetSore}/user/logout`).then((response) => {
      expect(response.status).to.eq(200);

    // Assert the structure of the response body
    expect(response.body).to.have.property('code', 200);
    expect(response.body).to.have.property('type', 'unknown');
    expect(response.body).to.have.property('message').that.includes('ok');
    });
  });
});