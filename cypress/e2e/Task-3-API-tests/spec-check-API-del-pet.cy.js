const { baseUrlPetSore } = Cypress.config();
import pet from "../../support/page_object/Task3_API_objects/pet_object";


describe('Pet Store API Tests', () => {

  it('allows adding a new Pet', () => {
    cy.request('POST', `${baseUrlPetSore}/pet`, pet).then((response) => {
      expect(response.status).to.eq(200);
    });
  });

  it('allows deleting Pet', () => {
    const petId = 7;

    cy.request('DELETE', `${baseUrlPetSore}/pet/${petId}`).then((response) => {
      expect(response.status).to.eq(200);
      });
    });
  });
