const { baseUrlPetSore } = Cypress.config();
import pet from "../../support/page_object/Task3_API_objects/pet_object";

describe('Add pet to the list', () => {

  it('allows adding a new Pet', () => {
    cy.request('POST', `${baseUrlPetSore}/pet`, pet).then((response) => {
      expect(response.status).to.eq(200);
    // Assert the updated pet details
    expect(response.body.id).to.equal(pet.id);
    expect(response.body.name).to.equal(pet.name);
    expect(response.body.status).to.equal('available');
    });
  });
});
