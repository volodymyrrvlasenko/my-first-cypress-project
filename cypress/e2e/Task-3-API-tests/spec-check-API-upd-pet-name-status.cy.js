const { baseUrlPetSore } = Cypress.config();
import pet from "../../support/page_object/Task3_API_objects/pet_object";
import updatedPet from "../../support/page_object/Task3_API_objects/updated_petId";
describe('API test updating pet', () => {

  it('should update the name and status of a pet', () => {

      // Create new pet
      cy.request('POST', `${baseUrlPetSore}/pet`, pet).then((response) => {
      expect(response.status).to.eq(200);

    // Make the API request to update the pet details

      cy.request('PUT', `${baseUrlPetSore}/pet`, updatedPet).then((response) => {
      expect(response.status).to.equal(200);

      // Assert the updated pet details
      expect(response.body.id).to.equal(updatedPet.id);
      expect(response.body.name).to.equal(updatedPet.name);
      expect(response.body.status).to.equal(updatedPet.status);
      });
    });
  });
});

