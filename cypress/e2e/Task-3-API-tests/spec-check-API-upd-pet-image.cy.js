const { baseUrlPetSore } = Cypress.config();
import pet from "../../support/page_object/Task3_API_objects/pet_object";

describe('Update pet image', () => {

  it('allows adding a new Pet', () => {

    cy.request('POST', `${baseUrlPetSore}/pet`, pet).then((response) => {
      expect(response.status).to.eq(200);
    });
  });
});


describe('Pet Store API Tests', () => {

  it('allows updating Pet’s image', () => {
    const petId = 7;

    // Read the image file as a blob
    cy.fixture('test_data/dog-puppy-on-garden.jpg', 'binary').then((imageContent) => {
      // Create FormData object
      const imageUpdateData = new FormData();

      // Append the image content as a blob to FormData
      imageUpdateData.append('file', new Blob([imageContent], { type: 'image/jpeg' }), 'image.jpg');

      cy.request({
        method: 'POST',
        url: `${baseUrlPetSore}/pet/${petId}/uploadImage`,
        body: imageUpdateData,
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      }).then((response) => {
        expect(response.status).to.eq(200);
      });
    });
  });
});