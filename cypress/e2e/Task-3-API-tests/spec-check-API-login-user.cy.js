const { baseUrlPetSore } = Cypress.config();

describe('Pet Store API Tests', () => {

  it('allows Log in User', () => {
    cy.request('GET', `${baseUrlPetSore}/user/login?username=testuser&password=password123`).then((response) => {
      expect(response.status).to.eq(200);

    // Assert the structure of the response body
    expect(response.body).to.have.property('code', 200);
    expect(response.body).to.have.property('type', 'unknown');
    expect(response.body).to.have.property('message').that.includes('logged in user session');
    });
  });
});