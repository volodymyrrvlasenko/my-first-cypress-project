import HomePageDWS from '../../support/page_object/Task2_objects/home_page_DWS.js';
import LoginPageDWS from '../../support/page_object/Task2_objects/login_page_DWS.js';

describe('Demowebshop Login', () => {
  let homePageDWS;
  let loginPageDWS;

  beforeEach(() => {
    homePageDWS = new HomePageDWS();
    loginPageDWS = new LoginPageDWS();
    homePageDWS.open();
  });

  it('should allow user login', () => {
    // Click on the login link
    homePageDWS.loginLink.click();

    // Fill in the login form with valid credentials
    loginPageDWS.loginForm.fill({
      loginEmail: 'john.milner@example.com',
      loginPassword: 'password123'
    });

    // Submit the login form
    loginPageDWS.submitLoginButton.click();

    // Check if the user's email link is present on the page
    loginPageDWS.headerLinks.contains('a', 'john.milner@example.com').should('exist');
  });
});
