import HomePageDWS from '../../support/page_object/Task2_objects/home_page_DWS.js';
import ApparelAndShoesPageDWS from '../../support/page_object/Task2_objects/apparel_and_shoes_page_DWS.js';
import Wishlist from '../../support/page_object/Task2_objects/wishlist_page_DWS.js';
import BlueSneaker from '../../support/page_object/Task2_objects/sneaker_page_DWS.js';

describe('Navigate to Apparel & Shoes and Click on Sneaker Image', () => {
  let homePageDWS;
  let apparelAndShoesPageDWS;
  let blueSneaker;
  let wishList;
  
  beforeEach(() => {
    homePageDWS = new HomePageDWS();
    apparelAndShoesPageDWS = new ApparelAndShoesPageDWS();
    blueSneaker = new BlueSneaker();
    wishList = new Wishlist();
    homePageDWS.open();
  });
  it('should navigate to Apparel & Shoes, click on the Sneaker image, add to wishlist, and verify wishlist quantity', () => {
    //Click Apparel&Shoes link
    homePageDWS.apparelAndShoes.click()

    // Wait for the Apparel & Shoes page to load
    cy.url().should('include', '/apparel-shoes');

    // Click on the Sneaker link
    apparelAndShoesPageDWS.blueAndGreenSneaker.click();

    // Click on the "Add to wishlist" button
    blueSneaker.addToWishListButton.click();

    // Check quantity in the Wishlist link
    homePageDWS.wishlistLinkQuantity.should('contain', '1');

    // Visit the Wishlist page
    homePageDWS.wishlistLink.first().click();

    // Verify that the wishlist quantity is updated to [1]
    wishList.wishListQuantity.should('contain', '(1)');
  });
});
