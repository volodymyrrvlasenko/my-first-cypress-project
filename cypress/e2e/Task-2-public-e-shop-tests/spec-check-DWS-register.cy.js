import HomePageDWS from '../../support/page_object/Task2_objects/home_page_DWS.js';
import RegisterPageDWS from '../../support/page_object/Task2_objects/register_page_DWS.js';



describe('Demowebshop Registration', () => {
  let homePageDWS;
  let registerPageDWS;

  beforeEach(() => {
    homePageDWS = new HomePageDWS();
    registerPageDWS = new RegisterPageDWS();
    homePageDWS.open();
  });  

  it('should allow user registration', () => {
    // Click on the register link
    homePageDWS.registerLink.click();
    // Fill in the registration form with dynamically generated data
    registerPageDWS.registerForm.fill({
      getGenderMale: 'True',
      getFirstName: 'John',
      getLastName: 'Dou',
      getEmail: 'email from RegisterForm',
      getPassword: 'qwerty123',
      getConfirmPassword: 'qwerty123'
    })

    // Submit the registration form
    registerPageDWS.submitRegisteButton.click();

    // Check if registration is successful
    registerPageDWS.registerResult.should('contain.text', 'Your registration completed');
  });
});
