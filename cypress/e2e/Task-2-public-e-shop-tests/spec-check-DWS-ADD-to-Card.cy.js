import HomePageDWS from '../../support/page_object/Task2_objects/home_page_DWS.js';
import ApparelAndShoesPageDWS from '../../support/page_object/Task2_objects/apparel_and_shoes_page_DWS.js';
import ShoppingCart from '../../support/page_object/Task2_objects/shopping_cart_page_DWS.js';
import BlueSneaker from '../../support/page_object/Task2_objects/sneaker_page_DWS.js';



describe('Add Item to Cart', () => {
  let homePageDWS;
  let apparelAndShoesPageDWS;
  let blueSneaker;
  let shoppingCart;
  
  beforeEach(() => {
    homePageDWS = new HomePageDWS();
    apparelAndShoesPageDWS = new ApparelAndShoesPageDWS();
    blueSneaker = new BlueSneaker();
    shoppingCart = new ShoppingCart();
    homePageDWS.open();
  });

  it('should navigate to the product page, add the item to the cart, and verify cart quantity', () => {
    //Click Apparel&Shoes link
    homePageDWS.apparelAndShoes.click()

    // Wait for the Apparel & Shoes page to load
    cy.url().should('include', '/apparel-shoes');

    // Click on the Sneaker link
    apparelAndShoesPageDWS.blueAndGreenSneaker.click();

    // Wait for the product details page to load
    cy.url().should('include', '/blue-and-green-sneaker');

    // Click on the "Add to cart" button
    blueSneaker.addToCartButton.click();

    // Check quantity in the Shopping cart link
    homePageDWS.ShopCartQuantity.should('contain', '1');

    // Visit the Shopping Cart page
    homePageDWS.shopCartLink.click();

    // Verify that the cart quantity is updated to [1]
    shoppingCart.shoppingItemsQty.should('contain', '(1)');
  });
});
