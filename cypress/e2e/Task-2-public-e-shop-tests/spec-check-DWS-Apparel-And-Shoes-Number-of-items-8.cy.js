import HomePageDWS from '../../support/page_object/Task2_objects/home_page_DWS.js';

describe('Change Number of Items on the Page', () => {
  let homePageDWS;
  
  beforeEach(() => {
    homePageDWS = new HomePageDWS();
    homePageDWS.open();
  });

  it('should change the number of items per page', () => {
    //Click Apparel&Shoes tab
    homePageDWS.apparelAndShoes.click()

    // Check the default number of items (e.g., 8)
    homePageDWS.numberOfItems.should('have.length', 8)
  });
});
