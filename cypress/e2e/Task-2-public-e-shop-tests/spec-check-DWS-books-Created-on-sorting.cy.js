import HomePageDWS from '../../support/page_object/Task2_objects/home_page_DWS.js';

describe('Sort Items on the Books Page', () => {
  let homePageDWS;
  
  beforeEach(() => {
    homePageDWS = new HomePageDWS();
    homePageDWS.open();
  });
  
  it('should sort items by Created on', () => {
    //Click Books tab
    homePageDWS.booksTab.click()

    // Select Price Low to High sorting
    homePageDWS.sortningDropdown.select('Created on');

    // Get the product items and their created dates
    homePageDWS.booksProductItems.should('have.length.greaterThan', 1).then(items => {
      const itemDates = Array.from(items, item => {
        const dateElement = item.querySelector('.created-on');
        return dateElement ? new Date(dateElement.textContent.trim()) : null;
      });

      // Check if the items are sorted by Created on
      const sortedDates = [...itemDates].sort((a, b) => (a > b ? 1 : a < b ? -1 : 0));
      expect(itemDates).to.deep.equal(sortedDates);
    });
  });
});
