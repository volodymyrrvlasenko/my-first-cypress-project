import HomePageDWS from '../../support/page_object/Task2_objects/home_page_DWS.js';

describe('Sort Items on the Books Page', () => {
  let homePageDWS;
  
  beforeEach(() => {
    homePageDWS = new HomePageDWS();
    homePageDWS.open();
  });
  it('should sort items by Price Low to High', () => {
    //Click Books tab
    homePageDWS.booksTab.click()

    // Select Price Low to High sorting
    homePageDWS.sortningDropdown.select('Price: Low to High');

    // Get the product items and their prices
    homePageDWS.booksProductItems.should('have.length.greaterThan', 1).then(items => {
      const itemPrices = Array.from(items, item => {
        const price = item.querySelector('.price.actual-price');
        return price ? parseFloat(price.textContent.replace('$', '').trim()) : 0;
      });

      // Check if the items are sorted by Price Low to High
      const sortedPrices = [...itemPrices].sort((a, b) => a - b);
      expect(itemPrices).to.deep.equal(sortedPrices);
    });
  });
});
