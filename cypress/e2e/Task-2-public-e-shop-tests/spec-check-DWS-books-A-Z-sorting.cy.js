import HomePageDWS from '../../support/page_object/Task2_objects/home_page_DWS.js';

describe('Sort Items on the Books Page', () => {
  let homePageDWS;
  
  beforeEach(() => {
    homePageDWS = new HomePageDWS();
    homePageDWS.open();
  });
  it('should sort items A-Z', () => {
    //Click Books tab
    homePageDWS.booksTab.click()

    // Select A-Z sorting
    homePageDWS.sortningDropdown.select('Name: A to Z');

    // Get the product items
    homePageDWS.booksProductItems.should('have.length.greaterThan', 1).then(items => {
      // Convert the items to an array of text
      const itemTexts = Array.from(items, item => item.innerText.trim());

      // Check if the items are sorted A-Z
      const sortedItems = [...itemTexts].sort();
      expect(itemTexts).to.deep.equal(sortedItems);
    });
  });
});
