import HomePageDWS from '../../support/page_object/Task2_objects/home_page_DWS.js';

describe('Sort Items on the Books Page', () => {
  let homePageDWS;
  
  beforeEach(() => {
    homePageDWS = new HomePageDWS();
    homePageDWS.open();
  });
  
  it('should sort items by Position', () => {
    //Click Books tab
    homePageDWS.booksTab.click()

    // Select Price Low to High sorting
    homePageDWS.sortningDropdown.select('Position');

    // Get the product items and their positions
    homePageDWS.booksProductItems.should('have.length.greaterThan', 1).then(items => {
      const itemPositions = Array.from(items, item => {
        const positionElement = item.querySelector('.position');
        return positionElement ? parseInt(positionElement.textContent.trim()) : null;
      });

      // Check if the items are sorted by Position
      const sortedPositions = [...itemPositions].sort((a, b) => a - b);
      expect(itemPositions).to.deep.equal(sortedPositions);
    });
  });
});
