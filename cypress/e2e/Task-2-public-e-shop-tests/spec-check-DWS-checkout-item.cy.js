import HomePageDWS from '../../support/page_object/Task2_objects/home_page_DWS.js';
import ApparelAndShoesPageDWS from '../../support/page_object/Task2_objects/apparel_and_shoes_page_DWS.js';
import ShoppingCart from '../../support/page_object/Task2_objects/shopping_cart_page_DWS.js';
import BlueSneaker from '../../support/page_object/Task2_objects/sneaker_page_DWS.js';
import LoginPageDWS from '../../support/page_object/Task2_objects/login_page_DWS.js';
import Checkout from '../../support/page_object/Task2_objects/checkout_page_DWS copy.js';

describe('Checkout item', () => {
  let homePageDWS;
  let apparelAndShoesPageDWS;
  let blueSneaker;
  let shoppingCart;
  let loginPageDWS;
  let checkout;
  
  beforeEach(() => {
    homePageDWS = new HomePageDWS();
    apparelAndShoesPageDWS = new ApparelAndShoesPageDWS();
    blueSneaker = new BlueSneaker();
    shoppingCart = new ShoppingCart();
    loginPageDWS = new LoginPageDWS();
    checkout = new Checkout();
    homePageDWS.open();
  });

  it('should navigate to the product page, add the item to the cart, and verify cart quantity and comple checkout flow', () => {
    // Click on the login link
    homePageDWS.loginLink.click();

    /// Fill in the login form with valid credentials
    loginPageDWS.loginForm.fill({
      loginEmail: 'john.milner@example.com',
      loginPassword: 'password123'
    });

    // Submit the login form
    loginPageDWS.submitLoginButton.click();

    // Check if the user's email link is present on the page
    loginPageDWS.headerLinks.contains('a', 'john.milner@example.com').should('exist');

    //Click Apparel&Shoes link
    homePageDWS.apparelAndShoes.click()

    // Wait for the Apparel & Shoes page to load
    cy.url().should('include', '/apparel-shoes');

    // Click on the Sneaker link
    apparelAndShoesPageDWS.blueAndGreenSneaker.click();

    // Wait for the product details page to load
    cy.url().should('include', '/blue-and-green-sneaker');

    // Click on the "Add to cart" button
    blueSneaker.addToCartButton.click();

    // Check quantity in the Shopping cart link
    homePageDWS.ShopCartQuantity.should('contain', '1');

    // Visit the Shopping Cart page
    homePageDWS.shopCartLink.click();

    // Verify that the cart quantity is updated to '(1)'
    shoppingCart.shoppingItemsQty.should('contain', '(1)');

    // Click checkout button
    shoppingCart.agrimentCheckBox.check();
    shoppingCart.CheckOutButton.click();

    // Click Contineue button for Billing Address
    checkout.BillAddressCointinueButton.should('be.visible').click();

    // Click Contineu button for Shipping Address
    checkout.ShipAddressCointinueButton.should('be.visible').click();

    // Click Continue button for shipping method
    checkout.ShipMethodCointinueButton.should('be.visible').click();

    // Click Continue button for Payment Method
    checkout.PayMethodCointinueButton.should('be.visible').click();

    // Click Continue button for Payment Information
    checkout.PayInfoCointinueButton.should('be.visible').click();

    // Click Confirm button for Confirm Order
    checkout.confirmButton.should('be.visible').click();

    // Verify succes checkout
    checkout.resultText
      .should('be.visible')
      .and('contain', 'Your order has been successfully processed!');

      cy.url().should('contain', 'checkout/completed/')
  });
});
