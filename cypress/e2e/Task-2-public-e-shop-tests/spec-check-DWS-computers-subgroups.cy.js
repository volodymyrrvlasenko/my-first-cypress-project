import HomePageDWS from '../../support/page_object/Task2_objects/home_page_DWS.js';

describe('Verify Computers Group Sub-Groups', () => {
  let homePageDWS;
  
  beforeEach(() => {
    homePageDWS = new HomePageDWS();
    homePageDWS.open();
  });

  it('should have Computers group with 3 sub-groups', () => {
    // Trigger hover over the 'Computers' link to reveal the sub-groups
    homePageDWS.topMenu.contains('a', 'Computers').trigger('mouseover');

    // Wait for the sub-groups to be visible
    homePageDWS.subGroups
      .should('be.visible')
      .and('have.length', 3);

    // Check if the sub-groups have correct names
    homePageDWS.subGroups.eq(0).should('contain.text', 'Desktops');
    homePageDWS.subGroups.eq(1).should('contain.text', 'Notebooks');
    homePageDWS.subGroups.eq(2).should('contain.text', 'Accessories');
  });
});
